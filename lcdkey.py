from PySide2.QtWidgets import QApplication, QMessageBox
from PySide2.QtUiTools import QUiLoader
from PySide2 import QtCore
from udp_serv import UDPServer
import json

color_released = "#FFFFFF"
color_down = "#FFA500"
class KeyStatus:
    released = 0
    pressed = 1
class KeyId:
    up = 0
    down = 1
    power = 2
    back = 3
    enter = 4

class Lcdkey(QtCore.QObject):
    def __init__(self) -> None:
        super(Lcdkey, self).__init__()
        self.ui = QUiLoader().load('lcdkey.ui')
        self.setKeyReleased(KeyId.up)
        self.setKeyReleased(KeyId.down)
        self.setKeyReleased(KeyId.power)
        self.setKeyReleased(KeyId.back)
        self.setKeyReleased(KeyId.enter)
        self.ui.installEventFilter(self)
        self.initKeys()
        self.exists = False
        self.udp = UDPServer()
        self.udp.callback = self.udp_handle
        self.udp.start()

    def initKeys(self):
        self.keyUp = KeyStatus.released
        self.keyDown = KeyStatus.released
        self.keyPower = KeyStatus.released
        self.keyEnter = KeyStatus.released
        self.keyBack = KeyStatus.released
        self.clearall()

    def show(self):
        self.ui.show()

    def clearall(self):
        self.ui.labelAlarmUp.setVisible(False)
        self.ui.labelAlarmDown.setVisible(False)
        self.ui.labelC.setVisible(False)
        self.ui.labelF.setVisible(False)
        self.ui.labelLel.setVisible(False)
        self.ui.labelPpm.setVisible(False)
        self.ui.labelSet.setVisible(False)
        self.ui.lcdMain.setVisible(False)
        self.ui.lcdTemp.setVisible(False)

    def eventFilter(self, obj, event):
        if self.exists:
            return True
        if obj is self.ui:
            self.keyEvent(event)
            # if event.type() == QtCore.QEvent.KeyPress:
            #     print(event.key())
        if event.type() == QtCore.QEvent.Close:
            self.exists = True
            return True
        return super(Lcdkey, self).eventFilter(obj, event)

    def keyEvent(self, event):
        if event.type() == QtCore.QEvent.KeyPress:
            if (event.isAutoRepeat() == False):
                self.keyPress(event.key())
                # print(event.key())
        elif event.type() == QtCore.QEvent.KeyRelease:
            if (event.isAutoRepeat() == False):
                self.keyRelease(event.key())

    def keyPress(self, key):
        if key == QtCore.Qt.Key_Up:
            self.keyUp = KeyStatus.pressed
            self.setKeyDown(KeyId.up)
        elif key == QtCore.Qt.Key_Down:
            self.keyDown = KeyStatus.pressed
            self.setKeyDown(KeyId.down)
        elif key == QtCore.Qt.Key_Left:
            self.keyBack = KeyStatus.pressed
            self.setKeyDown(KeyId.back)
        elif key == QtCore.Qt.Key_Right:
            self.keyEnter = KeyStatus.pressed
            self.setKeyDown(KeyId.enter)
        elif key == QtCore.Qt.Key_Return:
            self.keyPower = KeyStatus.pressed
            self.setKeyDown(KeyId.power)

    def keyRelease(self, key):
        if key == QtCore.Qt.Key_Up:
            self.keyUp = KeyStatus.released
            self.setKeyReleased(KeyId.up)
        elif key == QtCore.Qt.Key_Down:
            self.keyDown = KeyStatus.released
            self.setKeyReleased(KeyId.down)
        elif key == QtCore.Qt.Key_Left:
            self.keyBack = KeyStatus.released
            self.setKeyReleased(KeyId.back)
        elif key == QtCore.Qt.Key_Right:
            self.keyEnter = KeyStatus.released
            self.setKeyReleased(KeyId.enter)
        elif key == QtCore.Qt.Key_Return:
            self.keyPower = KeyStatus.released
            self.setKeyReleased(KeyId.power)

    def getKeyLabel(self, id):
        if id == KeyId.up:
            return self.ui.labelUp
        elif id == KeyId.down:
            return self.ui.labelDown
        elif id == KeyId.power:
            return self.ui.labelPower
        elif id == KeyId.back:
            return self.ui.labelBack
        elif id == KeyId.enter:
            return self.ui.labelEnter
        else:
            return None

    def setKeyDown(self, id):
        label = self.getKeyLabel(id)
        if label is None:
            return
        label.setStyleSheet("QLabel{background:" + color_down + ";}")

    def setKeyReleased(self, id):
        label = self.getKeyLabel(id)
        if label is None:
            return
        label.setStyleSheet("QLabel{background:" + color_released + ";}")

    def key_handle(self):
        obj = {
                  "msg": "key",
                  "result": {
                      "up": self.keyUp,
                      "down": self.keyDown,
                      "back": self.keyBack,
                      "enter": self.keyEnter,
                      "power": self.keyPower
                  }
              }
        return json.dumps(obj)
    
    def show_seg(self, seg):
        if seg == 0x20:     # T1
            self.ui.labelLel.setVisible(True)
        elif seg == 0x21:   # T2
            self.ui.labelPpm.setVisible(True)
        elif seg == 0x28:   # T9
            self.ui.labelSet.setVisible(True)
        elif seg == 0x2B:
            self.ui.labelAlarmUp.setVisible(True)
        elif seg == 0x2C:
            self.ui.labelAlarmDown.setVisible(True)
        elif seg == 0x29:
            self.ui.labelC.setVisible(True)
        elif seg == 0x2A:
            self.ui.labelF.setVisible(True)

    def lcd_handle(self, obj):
        try:
            self.clearall()
            if obj['main'] != '':
                self.ui.lcdMain.setVisible(True)
                self.ui.lcdMain.display(obj['main'])
            if obj['num'] != '':
                self.ui.lcdTemp.setVisible(True)
                self.ui.lcdTemp.display(obj['num'])
            for seg in obj['seg']:
                self.show_seg(seg)
        except:
            pass
        return '{"msg":"ok"}'

    def udp_handle(self, data):
        obj = json.loads(data)
        try:
            if obj['action'] == 'key':
                data = self.key_handle()
            elif obj['action'] == 'lcd':
                data = self.lcd_handle(obj['msg'])
        except:
            print('json error')
        return data

if __name__ == '__main__':
    app = QApplication([])
    lcdkey = Lcdkey()
    lcdkey.show()
    app.exec_()

