import socket
from threading import Thread

class UDPServer(object):

    address_family = socket.AF_INET
    socket_type = socket.SOCK_DGRAM
    server_address = ('', 10004)
    max_packet_size = 4096

    def __init__(self):
        self.callback = None
        self.socket = socket.socket(self.address_family, self.socket_type)
        self.socket.bind(self.server_address)

    def close(self):
        self.socket.close()

    def sendto(self, data, client_addr):
        self.socket.sendto(data.encode(encoding='utf-8'), client_addr)
    
    def recvfrom(self):
        data, client_addr = self.socket.recvfrom(self.max_packet_size)
        return data, client_addr

    def run(self):
        while True:
            data, client_addr = self.recvfrom()
            # callback
            if self.callback is not None:
                data = self.callback(data)
            self.sendto(data, client_addr)

    def start(self):
        self.thread = Thread(target=self.run)
        self.thread.setDaemon(True)
        self.thread.start()
