#### UDP协议

- 端口10004
- JSON格式

##### 服务器端

接收数据

```json
{
  "action": "key"
}
```

返回数据

```json
{
  "msg": "key",
  "result": {
    "up": 0,
    "down": 0,
    "back": 1,
    "power": 0,
    "enter": 1
  }
}
```

接收数据

```json
{
  "action": "lcd",
  "msg": {
    "main": "85.5",
    "num": "-10.5",
    "seg": [T13, T1]
  }
}
```

```c
enum {
    T1 = 0x20, T2, T3, T4, T5, T6, T7, T8,
    T9, T10, T11, T12, T13, T14, T15, P1, P2, SNUM
};
```

返回数据

```json
{
  "msg": "ok"
}
```

